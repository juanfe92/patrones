package objectpool;

import java.util.ArrayList;
import java.util.List;

import mundo.Disparo;

public class DisparoPool {
	
	private static DisparoPool instance;
	private int maxPoolSize;
	private List<Disparo> available = new ArrayList<>();
	private List<Disparo> inUse = new ArrayList<>();
	
	private DisparoPool() {
		setMaxPoolSize(2);
		for (int i = 0; i < maxPoolSize; i++) {
			Disparo disparo = new Disparo(0, 0);
			available.add(disparo);
		}
	}
	
	public static DisparoPool getInstance() {
    	if (instance == null) {
    		instance = new DisparoPool();
    	}
    	
    	return instance;
    }
	
	public Disparo acquireDisparo() {
		if (available.size() > 0 && available.size() <= maxPoolSize) {
			Disparo disparo = available.get(0);
			inUse.add(disparo);
			available.remove(disparo);
			return disparo;
		} else {
			return null;
		}
	}
	
	public void releaseDisparo(Disparo disparo) {
		if (inUse.contains(disparo) && inUse.size() > 0) {
			available.add(disparo);
			inUse.remove(disparo);
		}
	}
	
	private void setMaxPoolSize(int size) {
		this.maxPoolSize = size;
	}

}
