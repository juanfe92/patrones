package Decorator;

public interface IDecorator {

    String comportamientoNave();
    String comportamientEnemigo();
    
}
