package Decorator;

public class Decorator implements IDecorator{

    @Override
    public String comportamientoNave() {
        return "Disp N";
    }

    @Override
    public String comportamientEnemigo() {
        return "Disp E";
    }
    
}
