package singleton;

import java.io.File;

public class JugadorSingleton {
	
	private static File instance;
	
    public String value;

	private JugadorSingleton(String value) {
		this.value = value;
	}
    
    public static File getInstance(String value) {
    	if (instance == null) {
    		instance = new File(value);
    	}
    	
    	return instance;
    }

}
