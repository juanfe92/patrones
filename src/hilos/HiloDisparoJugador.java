package hilos;

import Decorator.Decorator;
import Decorator.IDecorator;
import State.GameContext;
import State.StateGames;
import interfaz.InterfazSpaceInvaders;
import mundo.Disparo;
import mundo.Enemigo;
import mundo.Partida;
import objectpool.DisparoPool;
import prototype.Hilo;

public class HiloDisparoJugador extends Hilo {

	private InterfazSpaceInvaders interfaz;
	private Enemigo[][] enemigos;
	private Partida actual;
	private Disparo disparo;

	public HiloDisparoJugador() {
		super();
	}

	public HiloDisparoJugador(InterfazSpaceInvaders b, Enemigo[][] c, Partida d, Disparo e) {
		// TODO Auto-generated constructor stub

		interfaz = b;
		enemigos = c;
		actual = d;
		disparo = e;

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		if (disparo != null) {
			
			GameContext gameContext = null;
			IDecorator decorator = null;
			String valor = "";
			int cont = 0;
			while (disparo != null && !disparo.getImpacto()) {
				interfaz.getPanelNivel().dibujarDisparo(disparo);
				disparo.shoot();

				for (int i = 0; i < enemigos.length && disparo != null
						&& !disparo.getImpacto(); i++) {
					for (int j = 0; j < enemigos[0].length && disparo != null
							&& !disparo.getImpacto(); j++) {
						if (disparo.hitsEnemigo(enemigos[i][j])) {
							disparo.setImpacto(true);
							interfaz.getEventos().notificar(enemigos[i][j].getPuntosPorMuerte());
							actual.eliminarUnEnemigo(true, enemigos[i][j]);
							gameContext = new GameContext();
							valor = gameContext.gameAction(StateGames.disparoEnemigo);
							decorator = new Decorator();
							interfaz.getPanelNivel().setLabelDisparoEnemigo(decorator.comportamientEnemigo() + " " + j);
							interfaz.getPanelNivel().repaint();
						}
					}
				}

				try {
					sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				interfaz.getPanelNivel().updateUI();

				if (disparo != null) {
					if (disparo.getPosY() <= 0) {
						disparo.setImpacto(true);
					}
				}
			}
			disparo.setPosX(-60);
			disparo.setPosY(-60);
			disparo.setImpacto(false);
			DisparoPool.getInstance().releaseDisparo(disparo);
		}
	}
	
	@Override
	public Hilo clonar() {
        HiloDisparoJugador objHilo = new HiloDisparoJugador();
        objHilo.setNombre( this.getNombre() );
		objHilo.setInterfaz(this.getInterfaz());
		objHilo.setPartidaEnemigos(this.getPartidaEnemigos());
		objHilo.setSpace(this.getSpace());
        return objHilo;
	}
	
}
