package State;

public class GameContext implements GameStatus{


    @Override
    public String gameAction(StateGames state) {
        if(state.equalsName("Enemigo")){
            return "Disp E";
        }else if(state.equalsName("Nave")){
            return "Disp N";
        }
        return null;
    }
    
}
