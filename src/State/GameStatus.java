package State;

public interface GameStatus {
    public String gameAction(StateGames state);
}
