package State;

public enum StateGames {
    disparoEnemigo("Enemigo"),
    disparoNave("Nave");

    private final String name;

    private StateGames(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    public String toString() {
        return this.name;
     }
    
}
