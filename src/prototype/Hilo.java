package prototype;

import interfaz.InterfazSpaceInvaders;
import mundo.Partida;
import mundo.SpaceInvaders;

public abstract class Hilo extends Thread{
    private Partida partidaEnemigos;
    private SpaceInvaders space;
    private InterfazSpaceInvaders interfaz;
    private String nombre;   

    public abstract Hilo clonar();

    @Override
	public void run() {
    }

    public Partida getPartidaEnemigos() {
        return partidaEnemigos;
    }

    public void setPartidaEnemigos(Partida partidaEnemigos) {
        this.partidaEnemigos = partidaEnemigos;
    }

    public SpaceInvaders getSpace() {
        return space;
    }

    public void setSpace(SpaceInvaders space) {
        this.space = space;
    }

    public InterfazSpaceInvaders getInterfaz() {
        return interfaz;
    }

    public void setInterfaz(InterfazSpaceInvaders interfaz) {
        this.interfaz = interfaz;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    

}
