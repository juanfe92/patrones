package prototype;

import java.util.Hashtable;

import hilos.HiloAnimacionEnemigos;
import hilos.HiloAuxiliarCreaDisparo;
import hilos.HiloDisparoEnemigos;
import hilos.HiloDisparoJugador;
import hilos.HiloEnemigos;

public class HiloPrototype {
    private Hashtable hashHilo = new Hashtable();

    public HiloPrototype(){
        Hilo hiloEnemigo = new HiloEnemigos();
        hiloEnemigo.setName("enemigo");
		addHilo("enemigo", hiloEnemigo);

        Hilo hiloAnimacion = new HiloAnimacionEnemigos();
        hiloEnemigo.setName("animacionEnemigo");
		addHilo("animacionEnemigo", hiloAnimacion);
    }

    public void addHilo(String nombre, Hilo hilo){
        this.hashHilo.put( nombre, hilo);
    }

    public Hilo getHilo(String nombre){
        Hilo objPrototipo = null;
        if(nombre.equals("crearDisparo")){
            objPrototipo = (HiloAuxiliarCreaDisparo) hashHilo.get( nombre );
        }else if(nombre.equals("disparoEnemigo")){
            objPrototipo = (HiloDisparoEnemigos) hashHilo.get( nombre );
        }else if(nombre.equals("animacionEnemigo")){
            objPrototipo = (HiloAnimacionEnemigos) hashHilo.get( nombre );
        }else if(nombre.equals("disparoJugador")){
            objPrototipo = (HiloDisparoJugador) hashHilo.get( nombre );
        }else{
            objPrototipo = (HiloEnemigos) hashHilo.get( nombre );
        }
        return objPrototipo;        
    }

    public Hilo getClon(String nombre){
        if(nombre.equals("crearDisparo")){
            Hilo objPrototipo = (HiloAuxiliarCreaDisparo) hashHilo.get( nombre );
            return (Hilo) objPrototipo.clonar();
        }else if(nombre.equals("disparoEnemigo")){
            Hilo objPrototipo = (HiloDisparoEnemigos) hashHilo.get( nombre );
            return (Hilo) objPrototipo.clonar();
        }else if(nombre.equals("animacionEnemigo")){
            Hilo objPrototipo = (HiloAnimacionEnemigos) hashHilo.get( nombre );
            return (Hilo) objPrototipo.clonar();
        }else if(nombre.equals("disparoJugador")){
            Hilo objPrototipo = (HiloDisparoJugador) hashHilo.get( nombre );
            return (Hilo) objPrototipo.clonar();
        }else{
            Hilo objPrototipo = (HiloEnemigos) hashHilo.get( nombre );
            return (Hilo) objPrototipo.clonar();
        }
    }


}
