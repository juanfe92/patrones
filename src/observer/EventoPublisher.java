package observer;

import java.util.ArrayList;
import java.util.List;

public class EventoPublisher {
	
	private List<DisparoSubscribe> observadores = new ArrayList<DisparoSubscribe>();
	
	public void agregarObservador(DisparoSubscribe observador) {
		observadores.add(observador);
	}
	
	public void eliminarObservador(DisparoSubscribe observador) {
		observadores.remove(observador);
	}	
	
	public void notificar(int incremento) {
		for (DisparoSubscribe observador : observadores) {
			observador.update(incremento);
		}
	}

}
