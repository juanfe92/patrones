package flyweight;

import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

public class TreeFactory {
	static Map<String, TreeType> treeTypes = new HashMap<>();
	
    public static TreeType getTreeType(String nombre, ImageIcon imageIcon, int x, int y) {
        TreeType result = treeTypes.get(nombre);
        if (result == null) {
            result = new TreeType(nombre, imageIcon, x, y);
            treeTypes.put(nombre, result);
        }
        return result;
    }
}
