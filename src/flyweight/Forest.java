package flyweight;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class Forest extends JFrame{
	private List<Tree> trees = new ArrayList<>();

    public List<Tree> plantTree(String nombre, ImageIcon imageIcon, int x, int y) {
        TreeType type = TreeFactory.getTreeType(nombre, imageIcon, x, y);
        Tree tree = new Tree(x, y, type);
        trees.add(tree);
        return trees;
    }

    @Override
    public void paint(Graphics graphics) {
        for (Tree tree : trees) {
            tree.draw(graphics);
        }
    }
}
