package flyweight;

import java.awt.Graphics;

import javax.swing.ImageIcon;

public class TreeType {
	
    private String nombre;
    private ImageIcon imageIcon;
    private int x;
    private int y;
    
    public TreeType(String nombre, ImageIcon imageIcon, int x, int y) {
        this.nombre = nombre;
        this.imageIcon = imageIcon;
        this.x = x;
        this.y = y;
    }

    public void draw(Graphics g, int x, int y) {
        g.drawImage(imageIcon.getImage(), x, y, null);
        //g.fillOval(x, y, 7, 7);
    }

}
